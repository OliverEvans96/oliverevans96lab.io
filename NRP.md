---
layout: page
title: NRP
short: nrp
siteimage: nrp.png
---
The PRP/NRP Pilot Engagement Team Meetings are bi-weekly on Mondays, 10:00A-11:00A PT

Please send [an email][me] containing the new director’s name/email/title and I will add them to the calendar invite and the distribution list for that group. 

CONNECTION DETAIL:

[2019 PRP/NRP Pilot Running Notes][notes]  

When: Monday, February 4, 2019 at 11:00 AM - 12:00 PM in (UTC-07:00) Mountain Time (US & Canada).

Location: [ZOOM][zoom]  

 <h2>PRP/NRP Meeting Agendas</h2>
 
- [2019, January 31 — 10:00AM Pacific - Agenda and attachements][2019-01-31]

[wral]: https://www.wraltechwire.com/2017/08/17/what-does-it-take-to-build-a-national-big-data-superhighway/
[me]: mailto:cchaplin@internet2.edu
[notes]: https://docs.google.com/document/d/1JfQO_odoUyYaNfCzRWz_cvrN2gtQj9ovbq9GJUkZyG0/edit
[zoom]: https://internet2.zoom.us/j/7203992979
[2019-01-31]: http://lsmarr.calit2.net/archive/PRP_Call__2019_01_31_Agenda.pdf