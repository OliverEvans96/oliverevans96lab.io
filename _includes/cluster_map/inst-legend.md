<div class="legend-container">
  <h3>Legend</h3>
  <div>
    <h4>Marker Size: Memory</h4>
    <table class="legend-table">
      <tr>
        <th>Marker Size</th>
        <th>Memory Range</th>
      </tr>
      {% for marker_size in site.data.cluster_map.inst_mem_legend %}
        <tr>
          <td>{{ marker_size[0] }}</td>
          <td>{{ marker_size[1] }}</td>
        </tr>
      {% endfor %}
    </table>
  </div>
  <div>
    <h4>Marker Color: CPUs</h4>
    <img src="/images/cluster_map/inst_cpu_legend.png" />
  </div>
  <div>
    <h4>Marker Label: GPUs</h4>
    <p>The number of GPUs is shown on each marker.</p>
  </div>
</div>
