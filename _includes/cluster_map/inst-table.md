<div class="data-table-container">
  <table class="data-table">
    <tr>
      <th class="institution">Institution</th>
      <th>Nodes</th>
      <th>CPUs</th>
      <th>GPUs</th>
      <th>Memory</th>
      <th>Storage</th>
      <th>State</th>
      <th>City</th>
    </tr>
    {% for institution in site.data.cluster_map.institutions %}
      <tr>
        <td class="institution">{{ institution.institution }}</td>
        <td>{{ institution.nodes }}</td>
        <td>{{ institution.cpus }}</td>
        <td>{{ institution.gpus }}</td>
        <td>{{ institution.memory }}</td>
        <td>{{ institution.storage }}</td>
        <td>{{ institution.state }}</td>
        <td>{{ institution.city }}</td>
      </tr>
    {% endfor %}
  </table>
</div>
