---
layout: namespace
title: Connect
date: 2018-12-20
name: connect
pi: Scott L. Sellars
institution: University of California, San Diego
software: Python, Tensorflow, sci-kit learn, numpy, redis
tagline: Machine Learning in Earth Sciences 
imagesrc: connect.png
categories: 
- "namespace" 
tags: [3D modeling]
---

Project Description: We are using a machine learning approach based on a 3D convolution neural 
network to segment and keep track of the entire life-cycle of earth science phenomena using NASA data.



