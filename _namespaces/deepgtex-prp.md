---
layout: namespace
title:  Deepgtex-prp
date: 2018-12-10
name: deepgtex-prp
pi: F. Alex Feltus, Professor of Genetics & Biochemistry
institution: Clemson University
software: python, conda, tensorflow-gpu, scikit-learn, numpy, argparse, matplotlib, halo, gene oracle
tagline: Deep Learning in Oncogenomics
imagesrc: deepgtex-prp.png
categories: 
- "namespace" 
tags: [Kubernetes]
---

The Feltus lab is running deep learning oncogenomics workflows on the Pacific Research Platform 
Kubernetes (K8s) cluster. The PRP K8s is allowing us to scale up our analyses by moving large 
genomics datasets between FIONA nodes and then screening tens of thousands of genes on GPUs 
for tumor biomarker discovery.
