---
layout: namespace
title: Viscomp
date: 2018-12-13
name: viscomp
pi: Ravi Ramamoorthi
institution: University of California, San Diego
software: python, tensorflow, pytorch, numpy
tagline: Deep Learning in Computer graphics
imagesrc: viscomp.png
categories: 
- "namespace" 
tags: []
---

We are using machine learning to accelerate and enhance computer graphics techniques. 
By using deep learning, a computer can learn a more effective way of generating an 
image with complex effects and materials than hand crafted algorithms allow.
