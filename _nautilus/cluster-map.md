---
layout: cluster_map
title: Cluster Map
short: cluster map
custom_css: cluster_map_styles
---

## By Institution
<div class="map-and-legend">
  <div class="map-container">
    <h3>Map</h3>
    {% google_map src="_data/cluster_map" groupby="institution" %}
  </div>
  {% include cluster_map/inst-legend.md %}
</div>

### Table of Data
{% include cluster_map/inst-table.md %}

## All Nodes
<div class="map-and-legend">
  <div class="map-container">
    <h3>Map</h3>
    {% google_map src="_data/cluster_map" groupby="node" %}
  </div>
  {% include cluster_map/node-legend.md %}
</div>

### Table of Data
{% include cluster_map/node-table.md %}

## Notes
The marker locations are approximate.
* I got the coordinates from a free [https://ipstack.com GeoIP API]; you get what you pay for sometimes.
* If city/region data is unavailable, USA markers default to somewhere in rural Kansas.
** See [https://www.theguardian.com/technology/2016/aug/09/maxmind-mapping-lawsuit-kansas-farm-ip-address this article] about a law suit filed by the unfortunate farmer living there.
** There is one node (<code>osg.kans.nrp.internet2.edu</code>) which is ''actually'' located in Kansas.
* I added a ~0.1 mile jitter to each marker so as to avoid 100% overlap if two markers have the same coordinates.

## Source Code
The python code used to create this page can be found in the [https://gitlab.nautilus.optiputer.net/nautilus-wiki/cluster-map GitLab repo].
