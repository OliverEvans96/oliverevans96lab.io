---
layout: post
title: PRP gridftp MaDDash Migrated
date: 2018-09-17
categories: 
- "news" 
tags: [Kubernetes]
imagesrc: pic02.jpg
---

The PRP gridftp MaDDash was migrated into a Kubernetes Hyperconverged cluster made from the [PRP DTNs][1]. 
This is part of an upgrade to the storage and RAM on all the PRP provided nodes. The PRP MaDDash has moved [here][2].

[1]: https://nautilus.optiputer.net/ 
[2]: https://perfsonar.nautilus.optiputer.net/maddash-webui/

