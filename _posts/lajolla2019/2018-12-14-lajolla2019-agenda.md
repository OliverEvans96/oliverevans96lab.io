---
layout: workshop
title: "La Jolla FIONA  Workshop Agenda"
date: 2018-12-14
workshop: lajolla2019
short: lajolla2019
img: lajolla2019/header.png
---

<a href="#day1" class="subtitle">March 16</a> | 
<a href="#day2" class="subtitle">March 17</a>

<table class="info">
<tr><td width="25%">
<div class="font-weight-bold text-info" id="instructors">Instructors</div>
Hervey Allen (HA), (<a href="https://nsrc.org/">NSRC</a>)<br />
John Hess (JH), (<a href="https://cenic.org/">CENIC</a>)<br />
Joel Polizzi (JP), (<a href="https://www.ucsd.edu/">UCSD</a>)<br />
</td>
<td>
<div class="font-weight-bold text-info" id="notes">Notes</div>
Particularly as the PRP FIONA Workshop is itself a collaborative development effort, the program
content, session/lab schedule, and instructors are subject to change. We expect the general flow and
cadence of the workshop to be as described below.<br>
All times are Pacific (PST -- UTC-08:00)
</td>
</tr>
</table>


<div class="font-weight-bold text-info" id="day1">Saturday, March 16 </div>
<table class="workshop">
<tr>
    <th>Time</th>
    <th>Activity</th>
</tr>
<tr>
  <td class="break">08:00 - 09:00 </td>
  <td class="break">Breakfast</td>
</tr>
<tr>
  <td>09:00 - 09:30</td>
  <td>Workshop Welcome and FIONette initial login; Workshop Slack channel (JH)</td>
</tr>
<tr>
    <td>09:30 - 10:30</td>
    <td><b>Session 1:</b> Network Performance Measurement Concepts: perfSONAR - Download: [<a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/raw/master/perfsonar/perfsonar-introduction.pdf">PDF</a> : <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/raw/master/perfsonar/perfsonar-introduction.pptx">PowerPoint<a/>] (HA)
    <ul><li>Jitter</li>
        <li>Types of Delay and their impact</li>
        <li>perfSONAR what it is</li>
        <li>perfSONAR ecosystem and what we are using</li>
        <li>How to measure reliability and throughput</li>
    </ul>
    <b>Session 2:</b> Explanation of our machine setup (HA)
    <br />
    <b>Lab 1:</b> CentOS Orientation and Configuration [<a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/systems/centos-initial-configuration.md">Lab</a>] (HA)
    </td>
</tr>
<tr>
  <td class="break">10:30 - 10:45</td>
  <td class="break">Break</td>
</tr>
<tr>
  <td>10:45 - 12:30</td>
  <td><b>Lab 2:</b> perfSONAR Lab (Install perfSONAR Testpoint Bundle) [<a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/perfsonar/exercises-perfsonar-testpoint.md">Lab</a>] (HA) 
      <br />
      <b>Lab 3:</b> Command line ad hoc network performance testing with perfSONAR tools [<a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/perfsonar/exercises-perfsonar-testpoint-bundle-use.md">Lab</a>] (HA)
      <br />
      <b>Lab 4:</b> (optional) SSH: Configure SSH Keys on FIONette [<a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/security/exercises-perfsonar-ssh-key.md">Lab</a>] (HA)</td>
</tr><tr>
  <td class="break">12:30 - 13:30</td>
  <td class="break">Lunch</td>
</tr>
<tr>
  <td>13:30 - 14:00</td>
  <td>Session 3: GridFTP: Presentation and Lab (Install GridFTP) (JH) </td>
</tr>
<tr>
  <td>14:00 - 15:00</td>
  <td>Session 4: MaDDash: Presentation and Lab (Install MaDDash and Central Measurement Archive) (JH)</td>
</tr>
<tr>
  <td class="break">15:00 - 15:15</td>
  <td class="break">Break</td>
</tr>
<tr>
  <td>15:15 - 17:00</td>
  <td>Session 5: perfSONAR and GridFTP mesh configuration: Presentation and Lab (Registering results, MaDDash visualization) (JH) </td>
</tr>
</table>
[Back to top]({{page.url}})
<br>

<div class="font-weight-bold text-info" id="day2">Sunday, March 17 </div>
<table class="workshop">
<tr>
    <th>Time</th>
    <th>Activity</th>
</tr>
<tr>
  <td class="break">08:00 - 09:00 </td>
  <td class="break">Breakfast</td>
</tr>
<tr>
  <td>09:00 - 10:30</td>
  <td>Session 1: Dashboard Roundup: Diagnose workshop testing and visualization environment;
      Identify & resolve issues affecting regular testing, registering results, visualization (JH)</td>
</tr>
<tr>
  <td class="break">10:30 - 10:45</td>
  <td class="break">Break</td>
</tr>
<tr>
  <td>10:45 - 12:00</td>
  <td>Session 2: Sys Admin & Security - Presentation and Lab (HA) </td>
</tr>
<tr>
  <td class="break">12:00 - 13:00</td>
  <td class="break">Lunch</td>
</tr>
<tr>
  <td>13:00 - 14:15</td>
  <td>Session 3: Science DMZ Architecture, Data Movement, DTNs and Security (HA)</td>
</tr>
<tr>
  <td class="break">14:15 - 14:30</td>
  <td class="break">Break</td>
</tr>
<tr>
  <td>14:30 - 16:00</td>
  <td>Session 4: Performance measurement and visualization deployment planning (JH)
  </td>
</tr>
</table>

[Back to top]({{page.url}})

