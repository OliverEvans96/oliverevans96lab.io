---
layout: workshop
title: "La Jolla Kubernetes-Grafana workshop agenda"
date: 2019-02-19
workshop: lajolla2019k8s
short: lajolla2019k8s
img: lajolla2019k8s/k8s.png
---

All materials presented during the workshop will be linked on this page.

If you are interested in doing hands-on exercises during the work shop please see [Materials](/lajolla2019k8s-materials)

<table class="workshop">
<tr>
    <th>Time</th>
    <th>Activity</th>
</tr>
<tr>
  <td>09:00 - 10:30</td>
  <td>Basic Kubernetes setup and functionality on the PRP</td>
</tr>
<tr>
  <td class="break">10:30 - 10:45</td>
  <td class="break">Break</td>
</tr>
<tr>
  <td>10:45 - 12:00</td>
  <td>Grafana for monitoring and measuring usage</td>
</tr>
<tr>
  <td class="break">12:00 - 13:00</td>
  <td class="break">Lunch and VROOM tech/image demos (Joel & Larry)</td>
</tr>
<tr>
  <td>13:00 - 14:00</td>
  <td>Policy writing for access, scheduling, monitoring, and measuring</td>
</tr>
<tr>
  <td>14:00 - 14:30</td>
  <td>Federating Kubernetes clusters</td>
</tr>
<tr>
  <td class="break">14:30 - 14:45</td>
  <td class="break">Break</td>
</tr>
<tr>
  <td>14:45 - 16:00</td>
  <td>Security in Kubernetes, CIlogon, etc.</td>
</tr>
</table>

[Back to top]({{page.url}})

