---
layout: collection-userdocs
title: GitLab
date: 2019-02-08
short: userdocs
categories: user
order: 1
---

We use our own installation of [GitLab](https://about.gitlab.com/what-is-gitlab/) for [Source Code Management](https://about.gitlab.com/product/source-code-management/), [Continuous Integration automation](https://about.gitlab.com/product/continuous-integration/), docker containers registry and other development lifecycle tasks. It fully uses Nautilus Cluster resources, which provides our users unlimited storage and fast builds.

##### Git repo

Start from registering at <https://gitlab.nautilus.optiputer.net>

You can use GitLab for storing your code like any other git repository. Here's [GitLab basics guide](https://docs.gitlab.com/ee/gitlab-basics/).

All data from our GitLab except container images is being nightly backed up to Google S3. 

##### Containers Registry

What makes GitLab especially useful for kubernetes cluster in integration with Docker Registry. You can store your containers directly in our cluster and avoid slow downloads from [DockerHub](https://www.docker.com) (although you're still free to do that as well). To use containers registry, [create a new project](https://docs.gitlab.com/ee/gitlab-basics/create-project.html) in your GitLab account and go to [Container Registry](https://docs.gitlab.com/ee/user/project/container_registry.html) tab. It's enabled by default for all projects.  

##### Continuous Integration automation

To fully unleash the GitLab powers, introduce yourself to [Continuous Integration automation](https://about.gitlab.com/product/continuous-integration/) by creating the `.gitlab-ci.yml` file in your project ([Quick start guide](https://docs.gitlab.com/ce/ci/quick_start/)). The runners are already configured. There's a list of CI templates for most common languages available.

If you need to build your Dockerfile and create a container from it, adjust this `.gitlab-ci.yml` template:

```yaml
image: docker:git

before_script:
  - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN gitlab-registry.nautilus.optiputer.net

stages:
  - build-and-push

build-and-push-job:
  stage: build-and-push
  tags:
    - build-as-docker
  script:
    - docker build -t gitlab-registry.nautilus.optiputer.net/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:${CI_COMMIT_SHA:0:8} .
    - docker tag gitlab-registry.nautilus.optiputer.net/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:${CI_COMMIT_SHA:0:8} gitlab-registry.nautilus.optiputer.net/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:latest
    - docker push gitlab-registry.nautilus.optiputer.net/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}
``` 

Go to `CI / CD -> Jobs` tab to see in amazement your job running and image being uploaded to your registry. You can get the URL to your image in Registry tab to be included in your pod definition as:

```yaml
spec:
  containers:
  - name: my-container
    image: gitlab-registry.nautilus.optiputer.net/<your_group>/<your_project>:<optional_tag>
``` 