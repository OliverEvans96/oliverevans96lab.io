---
layout: collection-userdocs
title: Private repo
date: 2019-01-10
short: userdocs
categories: user
order: 2
---

In case you want to provide access to container images stored in **private** [Nautilus GitLab][1] repository:

Go to your repository  **Settings->Repository->Deploy** Tokens, and [create a deploy token](https://docs.gitlab.com/ce/user/project/deploy_tokens/) 
with **read_registry** flag enabled.

After that follow the instructions for [pulling image from private registry][2].

__your-registry-server__ will be `gitlab-registry.nautilus.optiputer.net` or `gitlab-registry.nautilus.optiputer.net/user/repo`

[1]: https://gitlab.nautilus.optiputer.net
[2]: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/#create-a-secret-in-the-cluster-that-holds-your-authorization-token
