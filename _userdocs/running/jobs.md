---
layout: collection-userdocs
title: Large jobs
date: 2019-01-25
short: userdocs
categories: user
order: 7
---

If you need to run large-scale computations, it's highly recommended to use [Jobs](https://kubernetes.io/docs/concepts/workloads/controllers/jobs-run-to-completion/) to avoid the need to manually run your task inside the pods, and to manually kill the pods after the computation is done. It becomes the necessity in case you're using significant number of GPUs, since those can't be shared and are reserved to your PODs until you shut down those.

We have seen several successful examples of running large computations in our cluster, and can provide you guidance and examples based on your case. Please come talk to us in [RocketChat](https://rocket.nautilus.optiputer.net) about your use case.

