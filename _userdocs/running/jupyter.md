---
layout: collection-userdocs
title: Jupyter 
date: 2019-01-23
short: userdocs
categories: user
order: 1
---

We provide the [JupyterLab](https://jupyterhub.nautilus.optiputer.net) service running in our cluster, which is great if you need to quickly run your workflow and not want to learn any kubernetes. Please note that your jupyter container will shut down 1 hour after your browser disconnects from it. If you need your job to keep running, don't close the browser window.

In case you feel limited with what standard jupyterlab instance provides, you can run your own jupyter container in Nautilus. 


#### Step by Step: Tensorflow with Jupyter 

##### Set context
Let's set the default namespace to avoid typing it for every command.
Use your own namespace in the following command:
```bash
$ kubectl config set-context nautilus --namespace=<your new namespace>
```

Check the default context with
```bash
$ kubectl config get-contexts
```

#### Tensorflow deployment

1. Create a tensorflow deployment with one pod

```yaml
cat << EOF | kubectl create -f -
apiVersion: v1
kind: Pod
metadata:
  name: gpu-pod-example
spec:
  containers:
  - name: gpu-container
    image: gitlab-registry.nautilus.optiputer.net/prp/jupyterlab:latest
    args: ["sleep", "infinity"]
    resources:
      limits:
        nvidia.com/gpu: 1
EOF
```

1.  Check the container is running
```text
$ kubectl get pods
```
should give you the list of containers running in your namespace, including `gpu-pod-example`.

1. Interactively login into our tensorflow container
```bash
$ kubectl exec gpu-pod-example -it bash
```
The **-i** ask for bash to be executed interactively and **-t** to allocate a terminal. 
That is to say you want to see the output, and you want be able to interact with it.

1.  Run Jupyter notebook
In the newly running bash prompt running on the nautilus kubernetes container:
```bash
jovyan@gpu-pod-example:~$ jupyter notebook --ip='0.0.0.0'
```
Take note of the output from this command. It will output a line similar to the one below that we'll use to login:
```bash
[I 21:31:10.031 NotebookApp] http://(gpu-pod-example or 127.0.0.1):8888/?token=aadcb08050ece787180d8d832eb19b1efd48c64fa39b052d
```
1. Run port-forward to access the pod
In a separate terminal on your local machine run 
```bash
kubectl port-forward gpu-pod-example 8888:8888
```
Here we're telling kubectl to forward any request to localhost port 8888 from remote host port 8888.

1. Connect to Jupyter
open your browser to `locahost:8888` , enter the previously remembered token,
and you get a Jupyter notebook with access to tensorflow.

Once you are finished, we'll teardown our setup.

#### Teardown 
It is important to teardown everything once we are done with the Jupyter to free up resources for others. 

1. Close your Jupyter notebook tab once you are finished.
1. Shutdown the proxy  <br>
   This will stop local machine to be able to connect to the remote one. 
   Press `Ctrl-C` in the terminal where the proxy is running.
1. Shutdown the Jupyter Server<br>
   This will stop the running `jupyter notebook`, but will not free up the resources taken by our Pod.
   Press `Ctrl-C` twice on the terminal where jupyter server is running. You should see something along:
   ```bash
   Shutdown this notebook server (y/[n])? ^C
   stopping
   I 17:44:16.657 NotebookApp] Shutting down 2 kernels
   [I 17:44:22.181 NotebookApp] Kernel shutdown: 651a1901-da9b-4465-9bbb-1b77775ee5ca
   [I 17:44:22.191 NotebookApp] Kernel shutdown: 93b5f0d5-b2ba-44a4-af7e-fb46eb19dc48
   ```
   Exit the current remote shell and you should be back on your machine:
   ```bash
   root@gpu-pod-example:/notebooks# exit
   exit
   ```
1. Delete the pod<br>
   Check the pod is still running
   ```bash
   $ kubectl get pods
   NAME              READY     STATUS    RESTARTS   AGE
   gpu-pod-example   1/1       Running   0          36m
   ```
   Remove the pod
   ```bash
   $ kubectl delete pod gpu-pod-example
   ```

   Check the pod is terminating 
   ```bash
   $ kubectl get pods
   NAME              READY     STATUS        RESTARTS   AGE
   gpu-pod-example   1/1       Terminating   0          37m
   ```
   You can see the pod is currently being terminated under the `STATUS` column. If you retry after 
   a minute or so you should see no resources anymore.
   ```bash
   $ kubectl get pods
   No resources found.
   ```


[1]: https://jupyterhub.nautilus.optiputer.net
