---
layout: collection-userdocs
title: Kubeless
date: 2019-01-25
short: userdocs
categories: user
order: 5
---

#### Kubeless

kubeless is a Kubernetes-native serverless framework that lets you deploy small bits of code without having to worry about the underlying infrastructure plumbing. It leverages Kubernetes resources to provide auto-scaling, API routing, monitoring, troubleshooting and more.

Please refer to [Kubeless quick start][1] for documentation. The controller is deployed already, you should be able to directly deploy the functions. Start from installing the client on your machine.  

Currently we're waiting for some changes to be made in the kubeless client. Until then please use one of [described in this link][2]

[1]: https://kubeless.io/docs/quick-start/
[2]: https://yumrepo.nautilus.optiputer.net/kubeless/



