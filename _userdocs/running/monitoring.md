---
layout: collection-userdocs
title: Monitoring
date: 2019-01-25
short: userdocs
categories: user
order: 6
---

When you run your jobs, it's your responsibility to make sure they are running as intended, without [overusing the memory](/userdocs/start/best-practices/#mem_alloc), [not wasting resources](/userdocs/start/policies/#sched_gpu), and [not crashing the nodes](/userdocs/start/best-practices/). Our [Grafana](https://grafana.nautilus.optiputer.net/) page is a great resource to see what your jobs are doing.

You can see where your namespace stands in [overall cluster usage](https://grafana.nautilus.optiputer.net/d/KMsJWWPiz/cluster-usage?orgId=1), make sure your requested GPUs are [not wasted and are utilized well](https://grafana.nautilus.optiputer.net/d/fHSeM5Lmk/k8s-compute-resources-cluster-gpus?refresh=30s&orgId=1) (look in __GPU usage__ number in last table for your namespace, and drilldown to namespace to see misbehaving pods). Also make sure your CPU and Memory requests are not higher than [actual usage](https://grafana.nautilus.optiputer.net/d/efa86fd1d0c121a26444b636a3f509a8/k8s-compute-resources-cluster) for your namespace.