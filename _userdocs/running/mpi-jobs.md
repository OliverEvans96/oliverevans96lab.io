---
layout: collection-userdocs
title: MPI jobs 
date: 2019-01-10
short: userdocs
categories: user
order: 4
---

1. [Install helm][1] if not installed 
1. Follow the [guide in kube-openmpi project][2]
1. Add rolebindings:
   ```bash
   kubectl create -n YOUR_NAMESPACE -f https://gitlab.com/ucsd-prp/prp_k8s_config/blob/master/mpi/rolebindings.yaml
   ```
   Remember, you can only work within your own namespace. If something is not working, check that you have specified a namespace.

[1]: https://github.com/kubernetes/helm#install
[2]: https://github.com/everpeace/kube-openmpi#quick-start
