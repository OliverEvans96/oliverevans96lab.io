---
layout: collection-userdocs
title: Best Practices
date: 2019-01-23
short: userdocs
categories: user
order: 5
---

#### Memory allocation

The default Memory limit per container can be changed any pod in its YAML file.  
See kubernetes documentation on setting limits:
- [Specify memory request and memory limit][2]
- [Configure Default Memory Requests and Limits for a Namespace][3]


#### Use Tini

Tini script automatically catches SIGTERM and handles reading the exit status from processes, helping to avoid zombies. 
You can wrap your processes with tini in your Dockerfile :

<div class="codeblock"><pre>
RUN set -x &#92;
    && wget --quiet https://github.com/krallin/tini/releases/download/v0.10.0/tini &#92;
    && echo "1361527f39190a7338a0b434bd8c88ff7233ce7b9a4876f3315c22fce7eca1b0 *tini" | sha256sum -c - &#92;
    && mv tini /usr/local/bin/tini &#92;
    && chmod +x /usr/local/bin/tini

ENTRYPOINT ["tini", "--"]
CMD ["run-something"]
</pre></div>

For more info see [tini][1]

[1]: https://kubernetes.io/docs/tasks/administer-cluster/manage-resources/memory-default-namespace/
[2]: https://kubernetes.io/docs/tasks/configure-pod-container/assign-memory-resource/#specify-a-memory-request-and-a-memory-limit
[3]: https://kubernetes.io/docs/tasks/administer-cluster/manage-resources/memory-default-namespace/
