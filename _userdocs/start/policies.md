---
layout: collection-userdocs
title: Policies
date: 2019-01-10
short: userdocs
categories: user
order: 3
---

#### Namespace

One of the main concepts of running workloads in kubernetes is a namespace. A namespace creates an 
isolated environment in which you can run your pods. It is possible to invite other users to your namespace
and it is possible to have access to multiple namespaces.

#### Pod and Container

A Kubernetes pod is a group of containers that are deployed together on the same host. 
If you frequently deploy single containers, you can generally replace the word "pod" with "container".

#### Memory allocation
{: id="mem_alloc"}

The default Memory limit is  **4Gi** per container for most namespaces. You can increase it for a container if needed.
Each namespace has default limits per pod:
- 4GB memory **limit**
- 256MB memory **request**

A **request** is what will be reserved for your pod on a node. A **limit** is the maximum which your pod should never exceed. 
This can be changed  for any pod in its YAML file.  See [Best Practices][5]

If pod goes over the memory **limit**, the process in the pod taking the most memory will be hard 
killed (OOMKill). There's more discussion [here][4]  

While it's important to set the Limit properly, it's also important to not set the Request too high. 
The **requests** for GPU jobs are limited to **24GB x number of GPUs requested for all pods**.

In case you're using the Ceph storage, this causes a problem: SIGKILL leaves the zombie process and RBD 
device with hung I/O behind, which will prevent your pod from being stopped, and can only be fixed by rebooting the physical node.

While we are looking for a solution, be careful with your memory and don't exceed it.
Possible solutions:
- try  [preoomkiller][2]
- make sure your [init process is handling SIGTERM correctly][3]

#### CPU PODS

We do no support large scale multi node CPU deployments or jobs. Nautilus is currently a GPU cluster with storage. 
The CPUs are to support these workloads. We can add CPU compute nodes and create a new role if your group needs CPU compute support.

#### Scheduling GPUs
{: id="sched_gpu"}

When you request a number of GPUs for your pod, those become exclusive to it and nobody else can use those until you stop your pod. Because of that you should only schedule GPUs that you can actually use, and not allow GPU pods to idle for more than a couple hours.
If any GPU requested by your pod was utilized for less than 2% in the 6 hr period, you will get a notification email or the pod will be terminated.
GPUs are a limited resource shared by many users. If you plan on deploying
large jobs please present a plan in [rocketchat operators channel][1]

#### Scheduling PODs

We currently have several hundreds users in the system, and many of them leave behind their pods when their 
computation is done. There's no way for us to know whether some pod is useful or is abandoned. To clear the 
abandoned pods, there's periodic process which destroys the workloads created more than 2 weeks ago. In case 
you're running some service and would want us to keep it running, you can contact admins in RocketChat and ask 
for an exception. Please provide an estimated period of service functioning and brief description of what the 
service does.  For workloads not in exceptions list you will get 3 notifications after which your workload will 
be deleted. Any data in persistent volumes will remain.

[1]: https://rocket.nautilus.optiputer.net/channel/nautilus-ops
[2]: https://github.com/grosser/preoomkiller
[3]: https://engineeringblog.yelp.com/2016/01/dumb-init-an-init-for-docker.html
[4]: https://github.com/kubernetes/kubernetes/issues/50632
[5]: /userdocs/start/best-practices
