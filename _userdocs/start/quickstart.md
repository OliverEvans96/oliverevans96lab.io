---
layout: collection-userdocs
title: Quick Start 
date: 2019-01-10
short: userdocs
categories: user
order: 2
---

1. [Install][1] the kubectl tool
1. Login to [PRP Nautilus portal][2]  and click the **Get Config** link 
on top right corner of the page to get your configuration file
1. Save the file as **config** and put the file in your <home>/.kube folder. This
folder may not exists on your machine, to create it execute :
  ```bash
  mkdir ~/.kube
  ```
1. Make sure you are promoted from **guest** to **user**, and have a namespace
assigned to you. One way to verify is to **Namespaces** link under the
**Services** menu (while logged on the portal). If you are assigned to any
namespaces they will be listed in the output. 
1. Test kubectl can connect to the cluster using a command line tool : 
```bash
kubectl get pods -n your_namespace
```
It's possible there are no pods in your namespace yet.

1. Run **busybox** container in your namespace: 
```bash
kubectl run busybox -n your_namespace -it --rm --image=busybox -- sh 
```
This command will start a busybox container and login you in. It will quit once you log out from the console.

1. To learn more about kubernetes use the following links for the official documentation and
   tutorials.
   - [kubernetes basics][3]
   - [tutorials][5]
   - [kubectl tool][4]

1. [Proceed](/userdocs/running/toc-running/) to creating your first ML job in kubernetes

[1]: https://kubernetes.io/docs/tasks/tools/install-kubectl/
[2]: https://nautilus.optiputer.net
[3]: https://kubernetes.io/docs/tutorials/kubernetes-basics/
[4]: https://kubernetes.io/docs/reference/kubectl
[5]: https://kubernetes.io/docs/tutorials/

