---
layout: collection-userdocs
title: Nextcloud
date: 2019-02-08
short: userdocs
categories: user
order: 6
---

We provide access to the [Nextcloud][1] [instance][2] running in our cluster and using our ceph storage.
It's similar to other file sharing systems ([Dropbox](https://www.dropbox.com), [Google Drive](https://www.google.com/drive/) etc) and can be used to get data in the cluster, temporary stage the results, share data and so on. If you're planning to use it for large datasets, please contact us first with the usage plan.

[1]: https://nextcloud.com/
[2]: https://nextcloud.nautilus.optiputer.net
