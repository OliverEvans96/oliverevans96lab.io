---
layout: page
title: PRP Media
short: media
siteimage: media.png
---
<img border="0" src="/images/micro.png">
- <h3><a href="2018_summit.html" data-fancybox data-options='{"type" : "iframe", "iframe" : {"preload" : false, "css" : {"width" : "600px"}}}'>
2018 Summit on Applying Advanced Astronomy AI to Microscopy Workflows - Slides
  </a>
