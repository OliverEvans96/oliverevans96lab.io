---
layout: default
title: Nautilus documentation
short: nautilus documentation
siteimage: nautilus.png
custom_css: cluster_map_styles
---

<div class="post">
<h2>{{ page.title }}</h2>
<p>
Nautilus is a HyperCluster for running containerized Big Data
Applications. It is using  Kubernetes for managing and scaling containerized applications
and Rook for automating Ceph data services.<br>
The following pages provide more documentation for Nautilus cluster users and
administrators. 
</p>

<div class="border">
  <strong>You are ...</strong>
</div>
<br>
<p class="docrow">
<a href="/userdocs" class="button">Cluster user</a> &nbsp; &nbsp; &nbsp;
<a href="/admindocs" class="button">Cluster administrator</a> &nbsp; &nbsp;
<a href="/nautilus/namespaces" class="button">Browsing namespaces</a>
</p>

<div class="border">
  <strong>So... Where in the world is Nautilus?</strong>
</div>

<div class="map-and-legend" style="margin: 10px; margin-top: 40px;">
  <div class="map-container">
    <h3>Cluster Map (by institution)</h3>
      {% google_map height="450" width="600" src="_data/cluster_map" groupby="institution" %}
    </div>
    {% include cluster_map/inst-legend.md %}
</div>

  <p><a href="cluster-map">Click here</a> for a cluster map showing each individual node, as well as tables of data for both maps.</p>
</div>

