---
layout: page
title: Workshop Reports
short: reports
siteimage: reports.png
---
- [2nd NRP Workshop Report - 2018.09.20][nrp2]
- [DIBBs18 Workshop Report - Final Draft 2018.09.06][dibbs18]
- [Science Engagement: Best Practices & Scalability in the 21st Century by Trisha Gorman - 2018.03.20][nrpprp2018]
- [NRP Workshop Report - 2017.10.23][nrp2017]
- [Big Data Workshop Report - 2017.08.23][bdwr17]
- [Sellars Big Data Workshop - 2017.08.17][sellars]
- [PRP Workshop Report - 2017.02.21][prp2017]
- [Pacific Research Platform 2015 Workshop Report - 2015.12.16 ][prp2015]



[nrp2017]: http://prp.ucsd.edu/workshop-reports/NRP%20Workshop%20Report_FINAL-23-Oct-17.pdf/
[bdwr17]: http://prp.ucsd.edu/workshop-reports/BigDataWorkshop2017_Report_FINAL_082417.pdf
[prp2017]: http://prp.ucsd.edu/workshop-reports/prp-v2-workshop-report-2-21-17-final.pdf
[sellars]: http://prp.ucsd.edu/workshop-reports/Sellars%20BigDataWorkshop2017_Report_FINAL_082117.docx
[prp2015]: http://prp.ucsd.edu/workshop-reports/2015-prp-workshop-report/at_download/file
[dibbs18]: http://prp.ucsd.edu/workshop-reports/dibbs18-final-report-final-draft-9-6-2018/at_download/file
[nrpprp2018]: /images/reports/Science_Engagement--PRP-NRP_Report-FINAL_3-20-2018a.pdf
[nrp2]: http://dibbs18.ucsd.edu/images/2NRP_Workshop_Report_final-small-9-20-18.pdf
