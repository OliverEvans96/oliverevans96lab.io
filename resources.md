---
layout: page
title: PRP User Solution Resources
short: resources
siteimage: newsbckwrd.png
---
- [Fall 2018 FIONA build][fiona2018f]
- [Spring 2018 FIONA build][fiona2018]
- [Summer 2017 FIONA build][fiona2017]
- [Summer 2016 FIONA build][fiona2016]
- [Fall 2015 UCOP funded FIONA build][fiona2015]
- [FIONA DTN v2 Centos7][fionadtn]
- [UCSD PRP GridFTP Maddash][maddash]
- [CENIC PRP perfSONAR MaDDash][maddash2]
- [ESnet Fasterdata Knowledge Base][esnet]
- [Caltech Supercomputing Blog][caltechs]

[fiona2018f]:https://docs.google.com/spreadsheets/d/1b6EzbwMB36T9ndAmCsGMgcThJDSnVItcqj2RT6RwQ2o/edit#gid=1559789080
[fiona2018]: http://prp.ucsd.edu/solution-resources/spring-2018-fiona-build
[fiona2017]: http://prp.ucsd.edu/solution-resources/summer-2017-fiona-build
[fiona2016]: http://prp.ucsd.edu/solution-resources/summer-2016-fiona-build
[fiona2015]: http://prp.ucsd.edu/solution-resources/fall-2015-ucop-funded-fiona-build
[fionadtn]: http://prp.ucsd.edu/solution-resources/fiona-dtn-v2-centos7
[maddash]: http://prp.ucsd.edu/solution-resources/prp-maddash
[maddash2]: http://prp.ucsd.edu/solution-resources/cenic-perfsonar-maddash
[esnet]: http://prp.ucsd.edu/solution-resources/esnet-fasterdata-knowledge-base
[caltechs]: http://prp.ucsd.edu/solution-resources/caltech-supercomputing-blog